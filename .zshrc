[[ -s $HOME/.profile ]] && source $HOME/.profile

[[ -z $GRML_OSTYPE ]] && {
    local url=http://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
    [[ ! -f $HOME/.zshrc.grml ]] && curl -L -o $HOME/.zshrc.grml $url
    source $HOME/.zshrc.grml
}

type antigen | grep -q "shell function" || {
    [[ -f /usr/share/zsh/scripts/antigen/antigen.zsh ]] && {
        source /usr/share/zsh/scripts/antigen/antigen.zsh
    } || {

        [[ ! -f $HOME/.antigen/antigen.zsh ]] && {
            local url=https://raw.githubusercontent.com/zsh-users/antigen/master/antigen.zsh
            curl -L --create-dirs -o $HOME/.antigen/antigen.zsh $url
        }
        source $HOME/.antigen/antigen.zsh
    }
}
antigen bundle zsh-users/zsh-syntax-highlighting
antigen apply

ZSH_HIGHLIGHT_STYLES+=(
  default                       'fg=248'
  unknown-token                 'fg=196,bold,bg=234'
  reserved-word                 'fg=197,bold'
  alias                         'fg=190,bold'
  builtin                       'fg=107,bold'
  function                      'fg=85,bold'
  hashed-command                'fg=70'
  path                          'fg=30'
  globbing                      'fg=170,bold'
  history-expansion             'fg=blue'
  single-hyphen-option          'fg=240'
  double-hyphen-option          'fg=244'
  back-quoted-argument          'fg=220,bold'
  single-quoted-argument        'fg=137'
  double-quoted-argument        'fg=137'
  dollar-double-quoted-argument 'fg=148'
  back-double-quoted-argument   'fg=172,bold'
  assign                        'fg=240,bold'
)

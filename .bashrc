[[ -s $HOME/.profile ]] && source $HOME/.profile

# If not running interactively, don't do anything more
[[ $- != *i* ]] && return

[ $TERM == rxvt-unicode-256color ] && [ ! -f /usr/share/terminfo/r/rxvt-unicode-256color ] && {
    #apt-get install ncurses-term
    [ -f /usr/share/terminfo/r/rxvt-256color ] && export TERM=rxvt-256color
    [ -f /usr/share/terminfo/r/rxvt-unicode ] && export TERM=rxvt-unicode
}

#from https://github.com/geekless/dotfiles/blob/master/.bashrc
# history settings
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=5000
export HISTFILESIZE=$HISTSIZE
# append to the history file, don't overwrite it
shopt -s histappend
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

#colors
PS1='\[\e[0;32m\]\u@\h\[\e[m\]:\[\e[1;34m\]\w\[\e[m\]\[\e[1;32m\]\$\[\e[m\]\[\e[1;37m\]\[\e[0m\] '

# for freebsd/darwin
export CLICOLOR=1

ls --color=auto &> /dev/null && alias ls='ls --color=auto' ||
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
#pacman -S colordiff grc colorgcc
[[ -x `which colordiff 2>/dev/null` ]] && {
    alias diff='colordiff'
    [[ -x `which svn 2>/dev/null` ]] && alias svndiff='svn diff | colordiff'
}
[[ -x `which grc 2>/dev/null` ]] && {
    alias ping='grc ping'
    alias netstat='grc netstat'
    alias traceroute='grc traceroute'
    alias ifconfig='grc ifconfig'
}
#pacman -S source-highlight
[[ -x `which source-highlight 2>/dev/null` ]] && {
    for lang in sql python java html xml php; do
        alias source-highlight-$lang="source-highlight -s $lang -f esc256"
    done
}

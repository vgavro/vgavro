[[ -r /etc/profile ]] && source /etc/profile

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LESS="-R"
export EDITOR="vim"
export WORDCHARS="*?_-[]~=&;!#$%^(){}<>"

[[ `uname` == "Darwin" ]] && {
    # homebrew hacks
    export HOMEBREW_GITHUB_API_TOKEN=9564f382db62f7726a3a502c12742e2546acfbc8
    export PATH=$PATH:/usr/local/sbin
}
PATH="/usr/sbin:/sbin:/bin:/usr/games:$PATH"
[[ -d $HOME/.gem/ruby/2.2.0/bin ]] && PATH="$PATH:$HOME/.gem/ruby/2.2.0/bin"

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx

#!/bin/bash
set -x  # echo command before run

[ `whoami` != root ] && echo 'error: run only as root' && exit 1


echo "hostname [default: $(cat /etc/hostname 2> /dev/null)]" 
read _HOSTNAME
[ -n "$_HOSTNAME" ] && echo $_HOSTNAME > /etc/hostname

_LOCALTIME="Europe/Kiev"
ln -i -s /usr/share/zoneinfo/$_LOCALTIME /etc/localtime

sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf

rmmod pcspkr 2> /dev/null
echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf

sed -i 's/#Color/Color/g' /etc/pacman.conf

#yaourt and package tools
pacman --needed -S -yu base-devel wget
[ ! -x `which yaourt 2>/dev/null` ] && {
    wget -qO- https://raw.github.com/pbrisbin/aurget/master/aurget > /tmp/aurget
    bash /tmp/aurget -S --asroot --noedit package-query
    bash /tmp/aurget -S --asroot --noedit yaourt
}
yaourt --noconfirm -S pkgfile pkgtools
pkgfile --update

#https://wiki.archlinux.org/index.php/List_of_Applications

#command-line tools
yaourt --noconfirm -S zsh zsh-completions gvim stress tmux htop iotop hdparm lm_sensors strace nmap iptraf-ng tcpdump dhclient traceroute mtr dnsutils whois ncdu grc mc ranger multitail aria2 libqalculate colordiff source-highlight bash-completion imagemagick p7zip zip unzip unrar subversion git tig mercurial android-tools

#daemons
yaourt --noconfirm -S wicd openssh ntp
systemctl enable wicd sshd ntpd
ntpd -q && hwclock -w

#audio
yaourt --noconfirm -S pulseaudio pulseaudio-alsa paprefs pavucontrol alsa-utils mpd ncmpcpp
systemctl enable mpd

#x-server, tools and awesome
yaourt --noconfirm -S xorg-server xorg-drivers xorg-xinit xorg-apps awesome vicious blingbling-git xcompmgr scrot xdg-utils xdg-user-dirs perl-file-mimeinfo xlockmore

#fonts, terminus-cyrillic is patched terminus for proper cyrillic instead of community/terminus-font 4.38-2
yaourt --noconfirm -S ttf-dejavu ttf-freefont ttf-arphic-uming ttf-baekmuk ttf-ms-fonts terminus-cyrillic

#X tools
yaourt --noconfirm -S rxvt-unicode-pixbuf chromium firefox chromium-pepper-flash flashplugin vlc skype zathura zathura-djvu zathura-pdf-mupdf qiv meld gedit qbittorrent gthumb

#libreoffice
yaourt --noconfirm -S libreoffice-calc libreoffice-writer libreoffice-draw libreoffice-math libreoffice-impress libreoffice-base libreoffice-en-US

#yaourt --noconfirm -S android-sdk-platform-tools
#yaourt --noconfirm -S android-sdk android-sdk-build-tools

yaourt --noconfirm -S python2-virtualenv python2-pip python2-pillow python2-lxml python2-numpy ipython2 python2-ipdb

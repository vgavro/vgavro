#!/bin/bash

#perl-file-mimeinfo package should be installed so it worked without DE
#all will be opened through firefox if not
[ ! -d ~/.local/share/applications ] && mkdir ~/.local/share/applications
xdg-mime default chromium.desktop x-scheme-handler/http
xdg-mime default chromium.desktop x-scheme-handler/https
xdg-mime default zathura.desktop application/pdf
xdg-mime default qbittorrent.desktop application/x-bittorrent
xdg-mime default vlc.desktop `cat /usr/share/applications/mimeinfo.cache | cut -d= -f1 | grep video/`
xdg-mime default qiv.desktop `cat /usr/share/applications/mimeinfo.cache | cut -d= -f1 | grep image/`

#LC_ALL=C xdg-user-dirs-update # alredy in /etc/X11/xinit/xinitrc.d/

#should be created for ~/.config/mpd/mpd.conf
[ ! -d ~/.config/mpd/playlists ] && mkdir ~/.config/mpd/playlists
touch ~/.config/mpd/{database,log,pid,state,sticker.sql}

ranger --copy-config=scope

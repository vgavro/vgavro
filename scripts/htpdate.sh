#!/bin/bash
# workaround for strange bug +1 utc hour with htpdate
sudo timedatectl set-timezone UTC
sudo htpdate -s google.com
sudo timedatectl set-timezone Europe/Kiev
